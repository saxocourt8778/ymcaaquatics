//
//  ContentView.swift
//  YMCAAquatics
//
//  Created by Courtney Dittmer on 10/9/19.
//  Copyright © 2019 Courtney Dittmer. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection = 0
 
    var body: some View {
        TabView(selection: $selection){
            Text("Attendance")
                .font(.title)
                .tabItem {
                    VStack {
                        Image("Swimmer")
                    }
                }
                .tag(0)
            Text("Chemicals")
                .font(.title)
                .tabItem {
                    VStack {
                        Image("Chemical")
                    }
                }
                .tag(1)
            Text("Logs")
                .font(.title)
                .tabItem {
                    VStack {
                        Image("Log")
                    }
                }
                .tag(2)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
